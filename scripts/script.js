let arr = ['aaa','bbb','cccc','eeee'];
let  arr2 = [1,2,3,false]

function renderList (arg, parent = document.body) {

    let ul = document.createElement('ul')
     parent.append(ul);

    arg.forEach(element => {
        let li = document.createElement('li');
        li.textContent = element;
        ul.append(li)
    });
    
}

renderList(arr);
renderList(arr2);